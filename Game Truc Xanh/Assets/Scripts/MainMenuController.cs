using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip audioClip;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void Update()
    {
        audioSource.PlayOneShot(audioClip);
    }

    public void clickButton()
    {
        Application.LoadLevel("GamePlay");
    }
}
