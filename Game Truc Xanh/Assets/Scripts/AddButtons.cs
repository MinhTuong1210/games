using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddButtons : MonoBehaviour
{
    [SerializeField]
    private Transform pannel;
    [SerializeField]
    private GameObject Button;
    GameObject btn;

    void Awake()
    {
        for (int i = 0; i < 24; i++)
        {
            btn = Instantiate(Button);
            btn.name = "" + i;
            btn.transform.SetParent(pannel, false);
        }
    }
}
