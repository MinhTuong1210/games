using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Sprite backgroundImg;
    private List<Button> btnList = new List<Button>();
    [SerializeField]
    public Sprite[] SourceSprites;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip guestWrong,guestTrue;


    public List<Sprite> GameSprite = new List<Sprite>();
    private bool firstGuess, secondGuess;
    string firstName, secondName;
    int firstIndex, secondIndex, totalGuess, noOfGuess , correctGuess;


    void Awake()
    {
        SourceSprites = Resources.LoadAll<Sprite>("Sprites/GameImg");
    }
    void Start()
    {
        GetButton();
        totalGuess = btnList.Count / 2;
        AddListenner();
        AddSprites();
        Shuffle(GameSprite);
    }
    void AddSprites()
    {
        int size = btnList.Count;
        int index = 0;
        for (int i = 0; i < size; i++)
        {
            if (i == size / 2)
            {
                index = 0;
            }
            GameSprite.Add(SourceSprites[index]);
            index++;
        }
    }
    void GetButton()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("PuzzleButton");
        for (int i = 0; i < objects.Length; i++)
        {
            btnList.Add(objects[i].GetComponent<Button>());
            btnList[i].image.sprite = backgroundImg;
        }
    }
    void AddListenner()
    {
        foreach (Button btn in btnList)
        {
            btn.onClick.AddListener(() => PickPuzzle());
        }
    }
    // Update is called once per frame
    void PickPuzzle()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        if (!firstGuess)
        {
            firstGuess = true;
            firstIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            firstName = GameSprite[firstIndex].name;
            btnList[firstIndex].image.sprite = GameSprite[firstIndex];
        }
        else if(!secondGuess)
        {
            secondGuess = true;
            secondIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            secondName = GameSprite [secondIndex].name;
            btnList[secondIndex].image.sprite = GameSprite[secondIndex];
            noOfGuess++;
            StartCoroutine(CheckIfPuzzleMatched());
        }
    }
    IEnumerator CheckIfPuzzleMatched()
    {
        yield return new WaitForSeconds (1f);
        if (firstName == secondName && firstIndex != secondIndex)
        {
            correctGuess++;
            btnList[firstIndex].interactable = false;
            btnList[secondIndex].interactable = false;

            btnList[firstIndex].image.color = new Color(0, 0, 0, 0); 
            btnList[secondIndex].image.color = new Color(0, 0, 0, 0);
            checkIfFinished();
            audioSource.PlayOneShot(guestTrue);
        }
        else
        {
            //noOfGuess++;
            btnList[firstIndex].image.sprite = backgroundImg;
            btnList[secondIndex].image.sprite = backgroundImg;
            audioSource.PlayOneShot(guestWrong);
        }
        firstGuess = secondGuess = false;
    }  
    void checkIfFinished()
    {
        if(correctGuess == totalGuess)
        {
            Debug.Log("Win"+ noOfGuess);
        }
    }
    void Shuffle(List<Sprite> list)
    {
        Sprite temp;
        for(int i = 0;i< list.Count; i++)
        {
            temp = list[i];
            int random = Random.Range(i, list.Count);
            list[i] = list[random];
            list[random] = temp;
        }
    }
}
